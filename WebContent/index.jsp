<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello Servlet</title>
</head>
<body>

<p>Hello Servlet!</p>

<input type="button" value="Hello Servlet" onClick="location.href='HelloServlet'">
<input type="button" value="Welcome Servlet" onClick="location.href='welcome.jsp'">
<input type="button" value="Inquiry Servlet" onClick="location.href='inquiry.jsp'">
<input type="button" value="Mysql Servlet" onClick="location.href='MySqlServlet'">

<p>Get通信</p>
<!-- 送信される情報がURLに含まれる -->
<form method="get" action="TestServlet">
<input type="text" name="user_name">
<input type="password" name="password">
<input type="submit" value="Get">
</form>

<p>Post通信</p>
<!-- 送信される情報がURLに含まれない -->
<form method="post" action="TestServlet">
<input type="text" name="user_name">
<input type="password" name="password">
<input type="submit" value="Post">
</form>

</body>
</html>