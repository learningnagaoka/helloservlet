drop database if exists testdb;

create database testdb;
use testdb;
create table test_tb(
user_id int,
user_name varchar(255),
password varchar(255)
);

insert into test_tb values(1, "taro", "taro");
insert into test_tb values(2, "jiro", "jiro");
insert into test_tb values(3, "sabro", "sabro");