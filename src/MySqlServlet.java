

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/MySqlServlet")
public class MySqlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public MySqlServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		PrintWriter out = response.getWriter();

		out.println("<html>");
		out.println("<head>");
		out.println("<title> データベーステスト</title>");
		out.println("</head>");
		out.println("<body>");

		Connection con = null;
		String url = "jdbc:mysql://localhost/testdb";
		String user = "root";
		String password = "pass";

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection(url, user, password);
			Statement stm = con.createStatement();
			String sql = "SELECT * FROM test_tb";

			ResultSet rs = stm.executeQuery(sql);

			while(rs.next()) {
				int userId = rs.getInt("user_id");
				String user_name = rs.getString("user_name");
				String pass = rs.getString("password");
				out.println("<p>");
				out.println("UserId:"+ userId + "UserName:" + user_name + "Password:" + pass );
				out.println("</p>");

			}
			rs.close();
			stm.close();

		}catch(ClassNotFoundException e) {
			out.println("ClassNotFoundException:" + e.getMessage());
		}catch(SQLException e) {
			out.println("SQLException:" + e.getMessage());
		}catch(Exception e) {
			out.println("Exception:" + e.getMessage());
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch(SQLException e) {
				out.println("SQLException:" + e.getMessage());
			}
		}
		out.println("<body>");
		out.println("<html>");
	}
}
