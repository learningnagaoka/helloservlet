

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TestServlet() {
        super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		String name = request.getParameter("user_name");
		String pass = request.getParameter("password");

		System.out.println(name);
		System.out.println(pass);

		PrintWriter out = response.getWriter();
		out.println("<html><head></head><body>" + name + "<br>" + pass + "</body></html>");

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		String name = request.getParameter("user_name");
		String pass = request.getParameter("password");

		System.out.println(name);
		System.out.println(pass);

		PrintWriter out = response.getWriter();
		out.println("<html><head><body>" + name + "<br>" + pass + "</body></html>");
	}

}
